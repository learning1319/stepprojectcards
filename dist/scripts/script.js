const loginFormButton = document.querySelector(".loginForm-button");
const headerLoginButton = document.querySelector(".header_login-button");
const logoutButton = document.querySelector(".header_logout-button");
const inputEmail = document.querySelector('.loginForm-email');
const inputPassword = document.querySelector('.loginForm-password');
const loginOverlay = document.querySelector(".loginForm-overlay");
const loginForm = document.querySelector(".loginForm-form");
const filtersForm = document.querySelector(".form-filters");
const tokenCookie = getCookie("token");
const cardList = document.querySelector('.card-list');
const emptyCardList = document.createElement("p");


emptyCardList.classList.add("empty-list");
emptyCardList.innerText = "No items have been added";
 
cardList.appendChild(emptyCardList)
// const emptyCardList = document.querySelector(".empty-list");

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

function setCookieWithExpiration(name, value, hours) {    
    const expirationDate = new Date();
    const offsetHours = (new Date().getTimezoneOffset() / 60) * -1; 
    expirationDate.setTime(expirationDate.getTime() + ((hours + offsetHours) * 60 * 60 * 1000)); 
    const expires = "expires=" + expirationDate.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
    
}

  
  function loginChecker() {
    if (tokenCookie) {
        formRemover();
        updateButtonsAndList();
        // Hide login button when logged in
    } else {
        document.querySelector(".add-card").style.display = "none";
        document.querySelector(".empty-list").style.display = "none";
        headerLoginButton.style.display = "block"; // Show login button when logged out
    }
}
loginChecker();

function formRemover(){
     loginOverlay.classList.add("disabled");
    loginForm.classList.add("disabled");
    cleanerForm()
    
}
function updateButtonsAndList(){
    
    headerLoginButton.classList.add("disabled");
    logoutButton.classList.remove("disabled");
    filtersForm.classList.remove("disabled");
    emptyCardList.classList.remove("disabled");
    document.querySelector(".add-card").style.display = "flex";
    document.querySelector(".empty-list").style.display = "flex";
 
}
function cleanerForm(){
    inputEmail.value = "";
    inputPassword.value = "";
}

function falseLogin(){
    let incorectLogin = document.querySelector('.incorectLogin')
    if(incorectLogin){
    incorectLogin.remove()
    }
    const errorMessage = document.createElement("p");
    errorMessage.innerText = "Incorrect login or password";
    errorMessage.classList.add("incorectLogin");
    loginForm.appendChild(errorMessage);

    cleanerForm()
}


 


loginForm.addEventListener('keypress', function(event){
    if(event.key === "Enter"){
        event.preventDefault();
        submitLoginForm()
    }
})


document.addEventListener("click", function(event) {
   
    
    
    if(event.target === logoutButton && tokenCookie) {
        document.cookie = `token=; max-age= -1; path=/;`; 
        logoutButton.classList.add("disabled");
       
        headerLoginButton.classList.remove("disabled");
        filtersForm.classList.add("disabled");
        emptyCardList.classList.add("disabled");
        cardList.style.display = "none";
        addCardBtn.style.display = 'none'
    } else if(event.target === loginFormButton) {
        submitLoginForm();
        getAllCards()
    } else if(event.target === headerLoginButton) {
        loginOverlay.classList.remove("disabled");
        loginForm.classList.remove("disabled");
    } 
});

async function submitLoginForm(){
    const inputEmailValue = inputEmail.value;
    const inputPasswordValue = inputPassword.value;

    try {
        const response = await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email: inputEmailValue, password: inputPasswordValue })
        });
        
        if (!response.ok) {
            falseLogin()
            throw new Error('Network response was not ok');
        }

        const token = await response.text();
        setCookieWithExpiration("token", token, 8);
        formRemover();
        updateButtonsAndList();
        this.token = token;
        getAllCards();
    
    } catch (error) {
        console.error('There was a problem with your fetch operation:', error);
    }
}

// login script end



class Visit {
    constructor(params) {
        this.goal = params.goal;
        this.shortProp = params.shortProp;
        this.urgency = params.urgency;
        this.name = params.name;
    }

    createCardElement() {
        const cardElement = document.createElement('div');
        cardElement.classList.add('card');
        const editBtn = document.createElement('i')
        editBtn.classList.add('fa-solid', 'fa-pencil', 'edit-btn')
        const deleteBtn = document.createElement('i');
        deleteBtn.classList.add('fa-solid', 'fa-xmark', 'delete-btn');
        const cardBtns = document.createElement('div')
        cardBtns.classList.add('card-btns')

        const shortCardInfo = document.createElement('div');
        shortCardInfo.classList.add('short-card-info');
        shortCardInfo.innerHTML = `
            <h2>${this.name}</h2>
            <p>Goal: ${this.goal}</p>
            <p>Urgency: ${this.urgency}</p>
        `;

        const fullCardInfo = document.createElement('div');
        fullCardInfo.classList.add('full-card-info');
        fullCardInfo.innerHTML = `
            <h2>${this.name}</h2>
            <p>Goal: ${this.goal}</p>
            <p>Urgency: ${this.urgency}</p>
            <p>Description: ${this.shortProp}</p>
        `;

        const showMoreBtn = document.createElement('button');
        showMoreBtn.classList.add('show-more-btn');
        showMoreBtn.innerHTML = 'Show more';


        cardElement.appendChild(cardBtns)
        cardBtns.appendChild(editBtn)
        cardBtns.appendChild(deleteBtn);
        cardElement.appendChild(shortCardInfo);
        cardElement.appendChild(fullCardInfo);
        cardElement.appendChild(showMoreBtn);

        return cardElement;
    }
}

class Dentist extends Visit {
    constructor(params) {
        super(params);
        this.lastVisit = params.lastVisit;
        this.doctor = params.doctor
    }
}

class Therapist extends Visit {
    constructor(params) {
        super(params);
        this.age = params.age;
        this.doctor = params.doctor
    }
}

class Cardiologist extends Visit {
    constructor(params) {
        super(params);
        this.pressure = params.pressure;
        this.index = params.index;
        this.diseases = params.diseases;
        this.age = params.age;
        this.doctor = params.doctor
    }
}

const doctorHTML = document.querySelector('.doctor-name');
const modalCardiologist = document.querySelector('.modal-cardiologist');
const modalDentist = document.querySelector('.modal-dentist');
const modal = document.querySelector('.modal');
const cancelBTN = document.querySelector('.cancel-btn');
const confirmBTN = document.querySelector('.confirm-btn');


let currentDoctor;

doctorHTML.addEventListener('change', function () {
    const doctor = doctorHTML.value;
    console.log(doctor);
    if (doctor === 'dentist') {
        modalCardiologist.style.display = 'none';
        modalDentist.style.display = 'flex';
        age.style.display = 'none';
    } else if (doctor === 'cardiologist') {
        modalCardiologist.style.display = 'flex';
        age.style.display = 'flex';
        modalDentist.style.display = 'none';
    } else if (doctor === 'therapist') {
        age.style.display = 'flex';
        modalDentist.style.display = 'none';
        modalCardiologist.style.display = 'none';
    }
    currentDoctor = doctor;
});
function closeModal() {
    modal.style.display = 'none'
}

function openModal() {
    modal.style.display = 'flex';
}

const addCardBtn = document.querySelector('.add-card');


addCardBtn.addEventListener('click', function () {
    openModal()

});


const age = document.querySelector('.modal-age')
const ageHTML = document.querySelector('.age-input')
const nameHTML = document.querySelector('.name-input')
const pressureHTML = document.querySelector('.pressure-input')
const urgencyHTML = document.querySelector('.modal-urgency')
const descriptionHTML = document.querySelector('.shortdesc-input')
const goalHTML = document.querySelector('.goal-input')
const diseasesHTML = document.querySelector('.diseases-input')
const lastVisitHTML = document.querySelector('.lastVisit-input')
const indexHTML = document.querySelector('.index-input')

cancelBTN.addEventListener('click', function () {
    closeModal()
})

let cardId;

confirmBTN.addEventListener('click', function () {
    if (!['dentist', 'cardiologist', 'therapist'].includes(doctorHTML.value)) {
        alert('Choose the doctor');
        return;
    }

    const currentDoctor = doctorHTML.value;

    const doctorClass = getDoctorClass(currentDoctor);
    const card = new doctorClass({
        age: ageHTML.value,
        name: nameHTML.value,
        pressure: pressureHTML.value,
        shortProp: descriptionHTML.value,
        goal: goalHTML.value,
        urgency: urgencyHTML.value,
        index: indexHTML.value,
        lastVisit: lastVisitHTML.value,
        diseases: diseasesHTML.value,
        doctor: doctorHTML.value
    });

    if (card.age === '' ||
        card.name === '' ||
        card.pressure === '' ||
        card.goal === '' ||
        card.urgency === '' ||
        card.index === '' ||
        card.lastVisit === '' ||
        card.diseases === '') {
        alert('Fill all the data');
        return;
    }

    fetch('https://ajax.test-danit.com/api/v2/cards', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${tokenCookie}`
        },
        body: JSON.stringify(card),
    })
        .then(response => response.json())
        .then(data => {
            cardId = data.id;
            closeModal();
            console.log(data);
            createCard(card, currentDoctor, cardId);
        })
});

function getDoctorClass(doctor) {
    switch (doctor) {
        case 'dentist':
            return Dentist;
        case 'cardiologist':
            return Cardiologist;
        case 'therapist':
            return Therapist;
        default:
            throw new Error('In valid doctor type');
    }
}

function createCard(card, currentDoctor, cardId) {
    const cardElement = card.createCardElement();
    const showMoreBtn = cardElement.querySelector('.show-more-btn');
    const shortCardInfo = cardElement.querySelector('.short-card-info');
    const fullInfo = cardElement.querySelector('.full-card-info');
    const deleteBtn = cardElement.querySelector('.delete-btn');
    const editBtn = cardElement.querySelector('.edit-btn')

    const capitalizedDoctor = currentDoctor.charAt(0).toUpperCase() + currentDoctor.slice(1);
    cardList.appendChild(cardElement);
    console.log(card.name);

    shortCardInfo.innerHTML = `
        <h2>${capitalizedDoctor}</h2>
        <h2>${card.name}</h2>
    `;

    fullInfo.innerHTML = `
        <p>Goal: ${card.goal}</p>
        <p>Urgency: ${card.urgency}</p>
        <p>Description: ${card.shortProp}</p>
    `;

    if (card instanceof Dentist) {
        fullInfo.innerHTML += `<p>Last Visit: ${card.lastVisit}</p>`;
    } else if (card instanceof Therapist) {
        fullInfo.innerHTML += `<p>Age: ${card.age}</p>`;
    } else if (card instanceof Cardiologist) {
        fullInfo.innerHTML += `
            <p>Age: ${card.age}</p>
            <p>Pressure: ${card.pressure}</p>
            <p>Index: ${card.index}</p>
            <p>Diseases: ${card.diseases}</p>
        `;
    }

    const emptyList = document.querySelector('.empty-list');
    const cards = document.querySelectorAll('.card');

    if (cards.length < 1) {
        emptyList.style.display = 'flex';
    } else if (cards.length > 0 && emptyList) {
        emptyList.style.display = 'none';
    }

    showMoreBtn.addEventListener('click', function () {
        const allCards = document.querySelectorAll('.card');

        allCards.forEach(function (otherCard) {
            if (otherCard !== cardElement) {
                otherCard.classList.remove('show-less');
                otherCard.style.height = '150px';
                otherCard.querySelector('.full-card-info').style.display = 'none';
                otherCard.querySelector('.show-more-btn').innerHTML = 'Show more';
            }
        });

        if (cardElement.classList.contains('show-less')) {
            cardElement.classList.remove('show-less');
            fullInfo.style.display = 'none';
            cardElement.style.height = '150px';
            showMoreBtn.innerHTML = 'Show more';
        } else {
            cardElement.classList.add('show-less');
            fullInfo.style.display = 'flex';
            cardElement.style.height = 'auto';
            showMoreBtn.innerHTML = 'Show less';
        }

    });

    deleteBtn.addEventListener('click', function () {
        console.log(cardId);
        fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${tokenCookie}`
            },
        })
            .then(response => {
                if (response.ok) {
                    cardList.removeChild(cardElement);
                } else {
                    console.error('Failed to delete card');
                }
            })
            .catch(error => {
                console.error('Error deleting card:', error);
            });

    });
    editBtn.addEventListener('click', function () {
        fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${tokenCookie}`
            }
        })
        .then(response => response.json())
        .then(cardData => {
            const doctorClass = getDoctorClass(cardData.doctor);
            nameHTML.value = cardData.name;
            goalHTML.value = cardData.goal;
            urgencyHTML.value = cardData.urgency;
            descriptionHTML.value = cardData.shortProp;
            ageHTML.value = cardData.age || '';
            pressureHTML.value = cardData.pressure || '';
            indexHTML.value = cardData.index || '';
            diseasesHTML.value = cardData.diseases || '';
            lastVisitHTML.value = cardData.lastVisit || '';
            openModal();
    
            confirmBTN.style.display = 'none';

            const existingConfirmEditBtn = document.querySelector('.confirm-edit-btn');
        if (existingConfirmEditBtn) {
            existingConfirmEditBtn.parentNode.removeChild(existingConfirmEditBtn);
        }
            const confirmEditBtn = document.createElement('button');
            confirmEditBtn.textContent = 'Confirm Edit';
            confirmEditBtn.classList.add('confirm-edit-btn');
            const modalBtns = document.querySelector('.modal-btns');
            modalBtns.insertBefore(confirmEditBtn, modalBtns.firstChild);
            
            confirmEditBtn.addEventListener('click', function () {
                cardData.name = nameHTML.value;
                cardData.goal = goalHTML.value;
                cardData.urgency = urgencyHTML.value;
                cardData.shortProp = descriptionHTML.value;
                cardData.age = ageHTML.value;
                cardData.pressure = pressureHTML.value;
                cardData.index = indexHTML.value;
                cardData.diseases = diseasesHTML.value;
                cardData.lastVisit = lastVisitHTML.value;
                cardData.doctor = doctorHTML.value;
    
                fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${tokenCookie}`
                    },
                    body: JSON.stringify(cardData)
                })
                .then(response => {
                    if (response.ok) {
                        closeModal();
                        updateCard(cardData, cardElement);
                        confirmEditBtn.style.display = 'none'
                    } else {
                        console.error('Failed to update card');
                    }
                })
                .catch(error => {
                    console.error('Error updating card:', error);
                });
            });
        })
        .catch(error => {
            console.error('Error fetching card for editing:', error);
        });
    });
    
    function updateCard(cardData, cardElement) {
        
        cardElement.querySelector('.short-card-info').innerHTML = `<h2>${capitalizedDoctor}</h2>
        <h2>${cardData.name}</h2>`
        const fullInfo = cardElement.querySelector('.full-card-info');
        fullInfo.innerHTML = '';
        
        
        let additionalInfo = '';
    
        if (cardData.doctor === 'dentist') {
            additionalInfo = `<p>Last Visit: ${cardData.lastVisit}</p>`;
        } else if (cardData.doctor === 'therapist') {
            additionalInfo = `<p>Age: ${cardData.age}</p>`;
        } else if (cardData.doctor === 'cardiologist') {
            additionalInfo = `
                <p>Age: ${cardData.age}</p>
                <p>Pressure: ${cardData.pressure}</p>
                <p>Index: ${cardData.index}</p>
                <p>Diseases: ${cardData.diseases}</p>
            `;
        }
    
        fullInfo.innerHTML = `

            <p>Goal: ${cardData.goal}</p>
            <p>Urgency: ${cardData.urgency}</p>
            <p>Description: ${cardData.shortProp}</p>
            ${additionalInfo}
        `;
        closeModal();

    }
}
         
function getAllCards() {
            
console.log(tokenCookie)
            if (tokenCookie) {
                cardList.style.display = "grid";
                fetch('https://ajax.test-danit.com/api/v2/cards', {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${tokenCookie}`
                    },
                })
                    .then(response => response.json())
                    .then(data => {
                        console.log(data);
                        data.forEach(cardData => {
                            if (cardData.doctor && ['dentist', 'cardiologist', 'therapist'].includes(cardData.doctor)) {
                                const doctorClass = getDoctorClass(cardData.doctor);
                                const card = new doctorClass(cardData);
                                createCard(card, cardData.doctor, cardData.id);
                            } else {
                                console.error('Invalid doctor type:', cardData.doctor);
                            }
                        });
                    })
                    .catch(error => {
                        console.error('Error fetching cards:', error);
                    });
            } else {
                cardList.style.display = "none";
            }
        }
window.addEventListener('load', function () {
            getAllCards();
        });


const form = document.querySelector('.form-filters');
const searchInput = document.querySelector('.search');
const doctorSelect = document.querySelector('.doctor');
const urgencySelect = document.querySelector('.priority-visit');
const searchButton = document.querySelector('.search-button');

searchButton.addEventListener('click', filterCards);

function filterCards(event) {
    event.preventDefault();
  
    const searchValue = searchInput.value.toLowerCase();
    const doctorValue = doctorSelect.value.toLowerCase();
    const urgencyValue = urgencySelect.value.toLowerCase();

  
    fetch("https://ajax.test-danit.com/api/v2/cards", {
      method: 'GET',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${tokenCookie}`
      }
    }) 
      .then(response => response.json())
      .then(data => {
          const filteredCards = data.filter(card => {

          const nameMatch = !searchValue || card.name.toLowerCase().includes(searchValue);
          const doctorMatch = !doctorValue || doctorValue === '' || card.doctor === doctorValue;
          const urgencyMatch = !urgencyValue || urgencyValue === '' || calculateUrgencyLevel(card.urgency) === urgencyValue;
  
          return nameMatch && doctorMatch && urgencyMatch;
        });
       
        clearCardList();
  
        renderFilteredCards(filteredCards);
        
      })
      .catch(error => {
        console.error('Помилка при отриманні даних з сервера:', error);
      });
  }
  
  function clearCardList() { 
    cardList.innerHTML = '';
  }
  
  function renderFilteredCards(cards) {
        
    if (cards.length === 0) {
      cardList.innerHTML = '<p>Карток не найдено</p>';
    } else {
      cards.forEach(cardData => {
        if (cardData.doctor && ['dentist', 'cardiologist', 'therapist'].includes(cardData.doctor)) {
          const doctorClass = getDoctorClass(cardData.doctor);
          const card = new doctorClass(cardData);
          createCard(card, cardData.doctor, cardData.id);
        } else {
          console.error('Немає', cardData.doctor);
        }
      });
    }
  }
  
  function calculateUrgencyLevel(urgency) {
    if (urgency === 'Very urgent') {
      return 'high';
    } else if (urgency === 'Pretty urgent') {
      return 'normal';
    } else if (urgency === 'Not that urgent') {
      return 'low';
    } else {
      return '';
    }
  }